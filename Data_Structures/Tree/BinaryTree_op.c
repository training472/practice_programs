#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	int data;
	struct Node *left;
	struct Node *right;
} Node;

Node *createNode(int x)
{
	Node *newnode = (Node *)malloc(sizeof(Node));

	newnode->data = x;
	newnode->left = NULL;
	newnode->right = NULL;

	return newnode;
}

Node *insertLeft(Node *root, int x)
{
	Node *newnode = createNode(x);

	if (root == NULL)
		root = newnode;
	else
		root->left = insertLeft(root->left, x);

	return root;
}

Node *insertRight(Node *root, int x)
{
	Node *newnode = createNode(x);

	if (root == NULL)
		root = newnode;
	else
		root->right = insertRight(root->right, x);

	return root;
}

void inorder(Node *root)
{
	if (root == NULL)
		return;

	inorder(root->left);
	printf("%d ", root->data);
	inorder(root->right);
}

void preorder(Node *root)
{
	if (root == NULL)
		return;

	printf("%d ", root->data);
	preorder(root->left);
	preorder(root->right);
}

void postorder(Node *root)
{
	if (root == NULL)
		return;

	postorder(root->left);
	postorder(root->right);
	printf("%d ", root->data);
}

int main(void)
{
	Node *root = createNode(1);

	insertLeft(root, 10);
	insertRight(root, 20);

	insertLeft(root, 30);
        insertRight(root, 40);

	printf("Preorder: ");
	preorder(root); printf("\n");

	printf("Inorder: ");
        inorder(root); printf("\n");

	printf("Postorder: ");
        postorder(root); printf("\n");

	return 0;
}
