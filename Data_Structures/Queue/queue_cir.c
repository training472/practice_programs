#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max 5

int rear = -1, front = -1;
int q[max];

void display()
{
	if (front == -1 && rear == -1) {
		printf("Empty !!!\n");
		return;
	}

	int i = front;

	if (front <= rear) {
		for (int i = front; i <= rear; i++) {
			printf("%d ", q[i]);
		}
	}
	else {
		for (int i = front; i < max; i++)
			printf("%d ", q[i]);
		for (int i = 0; i <= rear; i++)
			printf("%d ", q[i]);
	}
	printf("\n");
}

void enqueue(int ele)
{
	if (front == (rear + 1) % max) {
		printf("Queue is full!! \n");
		return;
	}
	if (front == -1 && rear == -1) {
		rear++;
		front++;
	} else {
		rear = (rear + 1) % max;
	}

	q[rear] = ele;
}

int dequeue()
{
	int ele;
	if (front == -1 && rear == -1) {
		printf("Queue is empty!! \n");
		return -1;
	}
	if (front == rear) {
		ele = q[front];
		front = rear = -1;
	} else {
		ele = q[front];
		front = (front + 1) % max;
	}

	return ele;	
}

int main(void)
{
	//srand(time(0));

	int x;

	for (int i = 0; i < 4; i++) {
		x = rand() % 100;
		enqueue(x);
	}

	enqueue(9999);
	
	//dequeue();
	dequeue();
	enqueue(999);

	display();

	return 0;
}
