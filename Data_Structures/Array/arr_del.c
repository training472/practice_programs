#include <stdio.h>
#include "random.h"

int main(void)
{
	int n = 10, a[n];

	for (int i = 0; i < n; i++) {
		a[i] = gen_ran(100);
		printf("%d ", a[i]);
	}

	printf("\n");

	int x;

	scanf("%d", &x);

	int i = 0;
	while (a[i++] != x);
	
	for (;i < n; i++)
		a[i-1] = a[i];

	for (int j = 0; j < n-1; j++)
		printf("%d ", a[j]);

	printf("\n");

	return 0;
}
