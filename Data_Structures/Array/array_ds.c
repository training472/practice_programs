#include <stdio.h>

int main(void)
{
	int a[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	
	int i;
#if 0

	for(i = 0; i < 9; i++)
		printf("%d ", a[i]);

#endif

	while(i++ < 9)
		printf("%d ", a[i]);

	return 0;
}
