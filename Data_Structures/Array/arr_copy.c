#include <stdio.h>
#include <stdlib.h>

void reverse(int *a, int n)
{
	for (int i = 0; i < n/2; i++) {
		int temp = a[i];
		a[i] = a[n - i - 1];
		a[n - i - 1] = temp;
	}
}

int main(void)
{
	int n = 10, a[n], b[n];

	for (int i = 0; i < n; i++) {
		a[i] = rand() % 100;
		printf("%d ", a[i]);
	}
	printf("\n");

	for (int i = 0; i < n; i++)
		b[i] = a[i];

	for (int i = 0; i < n; i++)
		printf("%d ", b[i]);
	printf("\n");

	reverse(b, n);

	for (int i = 0; i < n; i++)
                printf("%d ", b[i]);
        printf("\n");

	return 0;
}
