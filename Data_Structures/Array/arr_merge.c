#include <stdio.h>
#include "random.h"

int main(void)
{
	int n = 10, a[n*2], b[n];

	for (int i = 0; i < n; i++) {
		a[i] = gen_ran(100);
		b[i] = gen_ran(100);
	}

	for (int i = n; i < 2*n; i++)
		a[i] = b[i - n];

	for (int i = 0; i < 2 * n; i++)
		printf("%d ", a[i]);

	printf("\n");

	return 0;
}
