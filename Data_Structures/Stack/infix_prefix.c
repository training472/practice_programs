#include <stdio.h>
#include <string.h>

#define MAX 100

int s[MAX];
int top = -1;

char peek()
{
	return s[top];
}

int isempty()
{
	return top == -1;
}

void push(char c)
{
	if (top == MAX - 1) {
		printf("Stack is full!! \n");
		return;
	}

	s[++top] = c;
}

char pop()
{
	if (isempty()) {
		printf("Stack is empty");
		return -1;
	}

	return s[top--];
}

int isOperand(char ch)
{
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}

int precedence(char c)
{
	switch (c) {
		case '+' : 
		case '-' : return 1;
		case '*' : 
		case '/' : return 2;
		case '^' : return 3;
	}
	return -1;
}

void str_rev(char *exp, int len)
{
	int i;
	for (i = 0; i < len/2; i++) {
		char temp = exp[i];
		exp[i] = exp[len - i - 1];
		exp[len - i - 1] = temp;
	}
}

void infixToPrefix(char *exp, int len)
{
	int i, j = 0;

	str_rev(exp, len);

	char prefix[len];

	for (i = 0; exp[i]; i++) {
		if (isOperand(exp[i]))
			prefix[j++] = exp[i];
		
		else if (exp[i] == '(')
			push(exp[i]);

		else if (exp[i] == ')') {
			while (!isempty() && peek() != '(')
				prefix[j++] = pop();
			if (!isempty() && peek() == '(' )
				pop();
		} else {
			while (!isempty() && precedence(exp[i]) <= precedence(peek()))
				prefix[j++] = pop();
			push(exp[i]);
		}
	}

	while (!isempty())
		prefix[j++] = pop();

	str_rev(prefix, len);

	printf("%s \n", prefix);
}

int main(void)
{
	char exp[] = "a+b*c+d";

	infixToPrefix(exp, strlen(exp));

	return 0;
}
