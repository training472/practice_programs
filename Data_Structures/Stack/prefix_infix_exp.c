#include <stdio.h>
#include <string.h>

#define MAX 100

char *st[MAX];
int top = -1;

int isempty()
{
	return top == -1;
}

void push(char *s)
{
	if (top == MAX - 1)
		return;

	strcpy(st[++top],s);
}

char *pop()
{
	if (isempty())
		return NULL;

	return st[top--];
}

void str_rev(char *exp, int n)
{
	for (int i = 0; i < n/2; i++) {
		int temp = exp[i];
		exp[i] = exp[n - i -1];
		exp[n - i - 1] = exp[i];
	}
}

int isOperand(char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

void prefixToInfix(char *exp, int n)
{
	int i;
	char infix[n];
	char op1[n], op2[n];

	str_rev(exp, n);

	for (i = 0; n; i++) {
		if (isOperand(exp[i])) {
			strcpy(op1,(char *)exp[i]);
			push(op1);
		}
		else {
			strcpy(op1, pop());
			strcpy(op2, pop());
			strcat(op1,"+");
			strcat(op1, op2);

			push(op1);
		}
	}

	strcpy(op1, pop());

	if (!isempty())
		printf("Invalid Expression!!\n");
	else
		puts(op1);
}

int main(void)
{
	char exp[] = "+a+*bcd";

	prefixToInfix(exp, strlen(exp));	

	return 0;
}
