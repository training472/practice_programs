#include <stdio.h>
#include <string.h>

#define MAX 100

char s[MAX];
int top = -1;

int isempty()
{
	return top == -1;
}

void push(char c)
{
	if (top == MAX - 1)
		return;

	s[++top] = c;
}

char pop()
{
	if (isempty())
		return -1;

	return s[top--];
}

void str_rev(char *exp, int n)
{
	for (int i = 0; i < n/2; i++) {
		int temp = exp[i];
		exp[i] = exp[n - i -1];
		exp[n - i - 1] = exp[i];
	}
}

int isOperand(char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

void prefixToInfix(char *exp, int n)
{
	int i, j = 0;
	char infix[n];


	for (i = 0; i < n; i++) {
		if (isOperand(exp[i])) {
			infix[j++] = exp[i];
			if (!isempty())
				infix[j++] = pop();
			else
				infix[j++] = '\0';
		}
		else
			push(exp[i]);
	}

	printf("%s \n", infix);
}

int main(void)
{
	char exp[] = "+a+*bcd";

	prefixToInfix(exp, strlen(exp));	

	return 0;
}
