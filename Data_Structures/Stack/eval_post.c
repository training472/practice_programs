#include <stdio.h>
#include <string.h>
#include <math.h>

#define max 100

int s[max];
int top = -1;

void push(int ele)
{
	if (top == max - 1)
		return;

	s[++top] = ele;

	printf("%d \n", ele);
}

int pop()
{
	if (top == -1)
		return -1;
	return s[top--];
}

int isOp(char c)
{
	return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
}

void eval_post(char *exp, int n)
{
	int op1, op2;

	for (int i = 0; i < n; i++) {
		if (!isOp(exp[i]))
			push(exp[i] - '0');
		else {
			op1 = pop();
			op2 = pop();

			switch (exp[i]) {
				case '+' : push(op1 + op2);
					   break;
				case '-' : push(op1 - op2);
					   break;
				case '*' : push(op1 * op2);
					   break;
				case '/' : push(op1 / op2);
					   break;
					   break;
			}
		}
		
	}

	printf("%d \n", pop());
}

int main(void)
{
	char *exp = "231*+9-";

	eval_post(exp, strlen(exp));

	return 0;
}
