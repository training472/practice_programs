#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX 10

int top = -1;
int s[MAX];

void display();

void push(int ele)
{
	if (top == MAX - 1) {
		printf("Stack is full!!! \n");
		return;
	}

	s[++top] = ele;

	display();
}

int pop()
{
	if (top == -1) {
		printf("Stack is empty!!!! \n");
	}

	s[top--];

	display();
}

void display()
{
	for (int i = top; i >= 0; i--)
		printf("%d ", s[i]);
	
	printf("\n");
}

int main(void)
{
	int x;

	srand(time(0));

	for (int i = 0; i < 8; i++) {
		x = rand() % 100;
		push(x);
	}

	pop();
	push(999);

	return 0;
}
