#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Node {
	int data;
	struct Node *next;	
} Node;

int findlen(Node *head)
{
	int len = 0;
	Node *ptr = head;

	while(ptr != NULL) {
		len++;
		ptr = ptr->next;
	}

	return len;
}

Node *createNode(int x)
{
	Node *newnode = (Node *)malloc(sizeof(Node));

	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

Node *insertAtEnd(Node *head, int x)
{
	Node *ptr = head;
	Node *newnode = createNode(x);

	if (head == NULL) {
		head = newnode;
		return head;
	}
	
	while (ptr->next != NULL)
		ptr = ptr->next;

	ptr->next = newnode;

	return head;	
}

Node *deleteAtEnd(Node *head)
{
	Node *ptr = head, *prev;

	if (head == NULL) {
		printf("Empty Linked list!! \n");
		return NULL;
	}

	while (ptr->next != NULL) {
		prev = ptr;
		ptr = ptr->next;
	}

	prev->next = NULL;

	return head;
}

Node *insertAtBegin(Node *head, int x)
{
	Node *newnode = createNode(x);

	if (head == NULL) {
		head = newnode;
		return head;
	}

	newnode->next = head;

	return newnode;
}

Node *deleteAtBegin(Node *head)
{
	Node *temp = head;

	if (head == NULL) {
		printf("Empty Linked List \n");
		return NULL;
	}

	temp = head;
	head = head->next;

	free(temp);

	return head;
}

Node *insertAtPosition(Node *head, int x, int pos)
{
	Node *ptr = head, *prev = NULL;
	int count = 1;

	Node *newnode = createNode(x);


	if (head == NULL) {
		printf("Invalid position!! \n");
		return NULL;
	}

	if (findlen(head) < pos) {
		printf("Invalid position!!!\n");
		return head;
	}

	while (count++ != pos) {
		if (ptr == NULL) {
			printf("Invalid Position...\n");
			return head;
		}
		prev = ptr;
		ptr = ptr->next;
	}

	newnode->next = prev->next;
	prev->next = newnode;

	return head;
}

Node *deleteAtPos(Node *head, int pos)
{
	Node *ptr = head, *prev = NULL;
	int count = 1;

	if (head == NULL) {
		printf("Invalid position!!! \n");
		return NULL;
	}

	if (findlen(head) < pos) {
		printf("Invalid Position!!! \n");
		return head;
	}
	
	while (count++ != pos && ptr != NULL) {
		if (ptr == NULL) {
                        printf("Invalid Position...\n");
                        return head;
                }
		prev = ptr;
		ptr = ptr->next;
	}

	prev->next = ptr->next;

	return head;
}

Node *deleteEle(Node *head, int ele)
{
	Node *ptr = head, *prev = NULL;

	int flag = 0;

	if (head == NULL) {
		printf("LinkedList is empty!!! \n");
		return NULL;
	}

	while (ptr != NULL && ptr->data != ele) {
		prev = ptr;
		ptr = ptr->next;
	}
	if (ptr != NULL) {
		if (ptr->data == ele) {
			prev->next = ptr->next;
			return head;
		}
	}
	else
		printf("%d not found...\n", ele);

	return head;
}

void reverseDisplay(Node *head)
{
	Node *ptr = head;

	if (ptr != NULL) {
		reverseDisplay(ptr->next);
		printf("%d ", ptr->data);
	}
}

Node *reverse(Node *head) 
{
	Node *next = NULL, *cur = head, *prev = NULL;
	Node *ptr = head;

	while (cur != NULL) {
		next = cur->next;
		cur->next = prev;
		prev = cur;
		cur = next;
	}

	head = prev;

	return head;
}

void display(Node *head)
{
	Node *ptr = head;

	while (ptr != NULL) {
		printf("%d ", ptr->data);
		ptr = ptr->next;
		if (ptr != NULL)
			printf("-> ");
	}

	printf("\n");
}

int main(void)
{
	
	Node *head = NULL;
	int x;

	//srand(time(0));

	for (int i = 0; i < 5; i++) {
		x = rand() % 100;
		head = insertAtEnd(head, x);
	}

	//head = insertAtEnd(head, 999);
	//head = insertAtEnd(head, 9999);

	display(head);

	//head = insertAtBegin(head, 999);
	//delete(head);
	//head = deleteAtBegin(head);
	//head = insertAtPosition(head, 999, 3);
	//head = deleteAtPos(head, 6);
	//head = deleteEle(head, 999);

	//reverseDisplay(head);
	head = reverse(head);

	display(head);

	return 0;
}
