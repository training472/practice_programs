#include <stdio.h>
#include <stdlib.h>

void display();

typedef struct Node {
	int data;
	struct Node *prev;
	struct Node *next;
} Node;

Node *head;

Node *createNode(int data) {
	Node *newnode = (Node *)malloc(sizeof(Node));

	newnode->data = data;
	newnode->prev = NULL;
	newnode->next = NULL;

	return newnode;
}

void insertEnd(int x)
{
	Node *ptr = head, *newnode = createNode(x);

	if (head == NULL) {
		head = newnode;
		return;
	}

	while (ptr->next != NULL) 
		ptr = ptr->next;

	ptr->next = newnode;
	newnode->prev = ptr;

	display();
}

void insertBegin(int x)
{
	Node *ptr = head, *newnode = createNode(x);

	if (head == NULL) {
		head = newnode;
		return;
	}

	newnode->next = ptr;
	ptr->prev = newnode;

	head = newnode;

	display();
}

void deleteEnd()
{
	Node *ptr = head;

	if (head == NULL)
		return;

	while (ptr->next != NULL)
		ptr = ptr->next;

	Node *temp = ptr;
	
	ptr->prev->next = NULL;

	free(temp);

	display();
}

void deleteBegin()
{
	if (head == NULL)
		return;

	Node *temp = head;

	head = head->next;
	head->prev = NULL;

	free(temp);

	display();
}

void deleteEle(int x)
{
	Node *ptr = head;

	if (head == NULL)
		return;

	while (ptr != NULL && ptr->data != x)
		ptr = ptr->next;

	if (ptr == NULL)
		return;

	if (ptr->data == x) {
		Node *temp = ptr;

		ptr->prev->next = ptr->next;

		free(ptr);
	}

	display();
}

void display()
{
	Node *ptr = head;
	if (head == NULL)
		return;
	
	while (ptr != NULL) {
		printf("%d ", ptr->data);
		ptr = ptr->next;
	}

	printf("\n");
}

int main(void)
{
	head = NULL;
	int x;

	for (int i = 0; i < 5; i++) {
		x = rand() % 100;
		insertEnd(x);
	}

	insertBegin(999);

	//deleteEnd();

	//deleteBegin();
	
	deleteEle(93);

	return 0;
}
