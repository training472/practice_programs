#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Node {
	int data;
	struct Node *next;	
} Node;

Node *top;

Node *createNode(int x)
{
	Node *newnode = (Node *)malloc(sizeof(Node));

	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

void push(int x)
{
	Node *newnode = createNode(x); 

	if (top == NULL) {
		top = newnode;
		return;
	}

	newnode->next = top->next;
	top->next = newnode;
}

void pop()
{
	Node *ptr = top;

	if (top == NULL) {
		printf("Stack is empty!!! \n");
		return;
	}

	ptr = top;
	top = ptr->next;

	free(ptr);
}

void display()
{
	Node *ptr = top;

	while (ptr != NULL) {
		printf("%d ", ptr->data);
		ptr = ptr->next;
	}
}

int main(void)
{
	int x;
	//srand(time(0));

	for (int i = 0; i < 5; i++) {
		x = rand() % 100;
		push(x);
	}

	display();
	printf("\n");

	pop();

	pop();
	display();
	printf("\n");

	return 0;
}
