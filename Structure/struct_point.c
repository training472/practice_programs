#include <stdio.h>

struct point {
	int x;
	int y;
};

int main(void)
{
	struct point *p, r;
	
	p = &r;

	p->x = 10;
	p->y = 20;

	printf("%d\n%d\n", r.x, r.y);
	
	return 0;
}
