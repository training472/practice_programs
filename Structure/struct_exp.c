#include <stdio.h>

struct name{
	char x: 5 ;
	char y: 3;
} b;

int main(void) {
	struct name a;
	
	printf("%d \n", a.x);
	printf("%c \n", a.y);

	printf("%d \n", sizeof(a));

	return 0;
}
