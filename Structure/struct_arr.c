#include <stdio.h>

struct info {
	char* name;
	int age;
} s[];

int main(void)
{
	struct info *p = &s;

	p->name = "aaaaa";
	p->age = 12;
	
	(p+1)->name = "bbbbb";
	(p+1)->age = 14;

	for (int i = 0; i < 2; i++) {
		printf("%s \n", s[i].name);
		printf("%d \n", s[i].age);
	}

	return 0;
}
