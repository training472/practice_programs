#include <stdio.h>

union name {
	int x: 1;
	int y: 2;
} a;

int main(void)
{
	printf("%ld\n", sizeof(a));

	return 0;
}
