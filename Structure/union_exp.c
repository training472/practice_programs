#include <stdio.h>

union test {
	int a;
	char *b;
	int c;
};

int main(void)
{
	union test t;

	t.b = "Hello";

	printf("%d \n", t.a);

	printf("Size: %ld \n", sizeof(t));	

	return 0;
}
