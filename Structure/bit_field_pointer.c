#include <stdio.h>

typedef struct Name {
	int x: 5;
	int y;
} Name;

int main(void)
{
	Name n;
	
	//int *p = &n.x;

	int *ptr = &n.y;

	//printf("%d \n", p->x);
	printf("%d \n", p->y);	

	return 0;
}
