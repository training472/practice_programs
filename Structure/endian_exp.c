#include <stdio.h>

int main(void)
{
	int data = 1;

	char *c = &data;

	if(*c)
		printf("Little Endian\n");
	
	if(!*c)
		printf("Big endian\n");

	return 0;
}
