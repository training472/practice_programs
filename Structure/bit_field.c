#include <stdio.h>

struct name {
	short int x: 1;
	short int y: 2;
};

int main(void)
{
	struct name a;
	
	a.x = 6;

	printf("%d \n", a.x);

	printf("%ld \n", sizeof(a));	

	return 0;
}
