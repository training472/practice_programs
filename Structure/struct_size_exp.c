#include <stdio.h>

typedef struct Tag {
	int x: 8;
} Tag;

int main(void)
{
	Tag t;

	printf("%ld \n", sizeof(t));

	return 0;
}
