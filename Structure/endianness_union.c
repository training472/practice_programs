#include <stdio.h>
//#include <inttypes.h>

typedef union Tag {
	int data;
	char buff[4];
} Tag;

int main(void)
{
	Tag t;

	t.data = 1;

	if(t.buff[0])
		printf("little endian \n");

	if(!t.buff[0])
		printf("big endian \n");

	return 0;
}
