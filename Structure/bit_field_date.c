#include <stdio.h>

typedef struct date {
	int d: 5;
	int m: 4;
	int y;
} date;

int main(void)
{
	date d;

	d.d = 16;
	d.m = 12;
	d.y = 2022;

	printf("%d/%d/%d\n", d.d, d.m, d.y);	

	return 0;
}
