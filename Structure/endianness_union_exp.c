#include <stdio.h>

typedef union Tag {
	int data;
	char buf[4];
} Tag;

int main(void)
{
	Tag t;

	t.data = 0x4F52;

	printf("%x \n", t.buf[0]);
	printf("%x \n", t.buf[1]);
	printf("%x \n", t.buf[2]);
	printf("%x \n", t.buf[3]);

	return 0;
}
