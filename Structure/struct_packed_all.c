#include <stdio.h>

#pragma pack(1)

typedef struct Tag1 {
	int a;
	char b;
	short c;
	char d;
} Tag1;

typedef struct Tag2 {
	char z;
	int a;
	char d;
} Tag2;

int main(void)
{
	printf("%ld \n%ld \n", sizeof(Tag1), sizeof(Tag2));

	return 0;
}
