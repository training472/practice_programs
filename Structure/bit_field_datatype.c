#include <stdio.h>

typedef struct Name {
	int x: 5 ;
	float y;
	char z[10]: 12;
} Name;

int main(void)
{
	Name n;

	n.x = 3;

	n.y = 1.1;

	printf("%f \n", n.y);

	return 0;
}
