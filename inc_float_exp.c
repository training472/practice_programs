#include <stdio.h>

int main(void) 
{
	float f = 1.0;
	long long int a = 0;
	while(f++ != 0)
		printf("%lf %lld \n", f, a++);
	
	printf("%lf \n", f);
}
