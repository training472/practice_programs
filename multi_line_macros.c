#include <stdio.h>
#include <stdlib.h>

/*#define PRINT(i, str) { \
		printf("%d ", i); \
		printf(#str); \
		printf("\n"); \
	}*/

#define PRINT(a) { \
		if(a % 2) \
			printf("%d ODD \n", a); \
		else \
			printf("%d EVEN \n", a); \
	}

int main(int argc, char* argv[]) {
	int a = atoi(argv[1]);
	
/*	if(a % 2) {
		PRINT(a, ODD);
	}	
	else 
		PRINT(a, EVEN);
*/

	PRINT(a);
	return 0;
}
