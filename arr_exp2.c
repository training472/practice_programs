#include <stdio.h>

int main(void)
{
	int a[2][2][2][2] = {1,2,3,4,5,6,7,8,1,2,3,4,5,6,7,8};

	int *ptr = &a;
	int **ptr1 = &ptr;
	int ***ptr2 = &ptr1;
	int ****ptr3 = &ptr2;

	for(int i = 0; i < 2; i++) {
		for(int j = 0; j < 2; j++) {
			for(int k = 0; k < 2; k++) {
				for(int l = 0; l < 2; l++) {
					printf("%d ", a[i][j][k][l]);
				}
				printf("\n");
			}
			printf("\n");
		}
		printf("\n");
	}

	//printf("%d \n", a[1][1][0][1]);

	printf("%p \n", *(*(*(*(ptr3+1)+1)+0)+1));
	
	return 0;
}
