#include <stdio.h>

int main(void)
{
	int a = 0x12345678;

	int y = (a >> 24) ^ (a << 24) | (a & ~0xFF) ^ (a & ~(0xFF << 24));

	printf("%x ", y);

	return 0;
}
