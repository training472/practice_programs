#include <stdio.h>

int main(void) {
	int a = 1, b = 2;

	if(a) {
		printf("Before break \n");
		break;
		printf("After break \n");
	}

	return 0;
}
