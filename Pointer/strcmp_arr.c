#include <stdio.h>

#if 0

int my_strcmp(char *a,char *b)
{
	int i = 0;
	while(a[i++] == b[i++]) {
		if(a[i] == '\0')
			return 0;
	}	

	return a[i] - b[i];
}

#endif

#if 1

int my_strcmp(char *a, char *b)
{
	while(*a++ == *b++) {
		if(*a == '\0')
			return 0;
	}	

	return a - b;
}

#endif

int main(void)
{
#if 0
	char a[] = "welcome";
	char b[] = "abghgbd";	
#endif

#if 1
	char *a = "welcome";
	char *b = "string";
#endif
	if(my_strcmp(a, b) == 0)
		printf("same \n");
	else
		printf("different \n");
	
	return 0;
}
