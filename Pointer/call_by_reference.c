#include <stdio.h>

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

int main(void)
{
	int x = 10, y = 20;

	swap(&x, &y);
	printf("x: %d\ny: %d\n", x, y);

	return 0;
}
