#include <stdio.h>
#include <string.h>

void my_strcpy(char *s, char *t)
{
	int i = 0;

	while((t[i] = s[i]) != '\0')
		i++;
}

int main(void)
{
	char str[] = "Hello world";
	char tar[strlen(str)];
	my_strcpy(str, tar);	
	
	printf("%s \n", tar);

	return 0;
}
