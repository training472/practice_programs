#include <stdio.h>
#include <string.h>
void my_strcpy(char *s, char *t)
{
	while((*t++ = *s++) != '\0');
}

int main(void)
{
	char *str = "Hello World";
	char tar[strlen(str)];

	my_strcpy(str, tar);

	printf("%s \n", tar);

	return 0;
}
