#include <stdio.h>

void modify(char *str)
{
	while(*str++ != '\0') {
		if(*str == 'e')
			*str = '9';
	}
}

int main(void)
{
#if 0
	char str[] = "welcome";
#endif

#if 1
	char *s = "welcome";
#endif

#if 0
	modify(str);
#endif

#if 1
	modify(s);
#endif
	printf("%s \n", s);
}
