#include <stdio.h>

int my_strlen(char *s)
{
	int len = 0;
	
	while(*s++ != '\0')
		len++;	

	return len;
}

int main(void)
{
	char *s = "String length";
	
	printf("%d \n", my_strlen(s));	

	return 0;
}
