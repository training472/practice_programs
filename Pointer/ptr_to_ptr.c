#include <stdio.h>

int main(void)
{
	int a = 100;
	int *ptr = &a;
	int **pp = &ptr;

	printf("a: %d \n", a);
	printf("ptr: %ld value: %d \n", ptr, *ptr);
	printf("*pp: %ld value: %d \n", pp, **pp);	

	return 0;
}
