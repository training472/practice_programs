#include <stdio.h>

int main(void)
{
	int x = 1, y = 2, z[10];
	int *ip;

	ip = &x;
	y = *ip;
	*ip = 0;
	ip = &z[0];

	printf("x: %d\n y: %d\n z[0]: %d\n", x, y, *ip);	

	return 0;
}
