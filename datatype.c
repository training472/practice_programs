#include <stdio.h>

int main(void) {
	char a = 'A';
	int b = 10;
	float c = 10.101;
	double d = 10.10101010101010101010101;
	short int e = 10;
	long int f = 10;
//	short short int g = 10;			//Gives error
//	short long int h = 10;			//Gives error
//	long short int i = 10; 			//Gives error
	long long int j = 10;
	long double k = 10.10;

	printf("char: %d \n", sizeof(a));
	printf("int: %d \n", sizeof(b));
	printf("float: %d \n", sizeof(c));
	printf("double: %d \n", sizeof(d));
	printf("short: %d \n", sizeof(e));
	printf("long: %d \n", sizeof(f));
//	printf("short short: %d \n", sizeof(g));
//	printf("short long: %d \n", sizeof(h));
//	printf("long short: %d \n", sizeof(i));
	printf("long long: %d \n", sizeof(j));
	printf("long double: %d \n", sizeof(k));

	return 0;
}
