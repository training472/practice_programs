#include <stdio.h>

void low_up(char s[]) {
	int i = 0;
	while(s[i] != '\0') {
		if(s[i] >= 'a' && s[i] <= 'z')
			printf("%c", s[i]+'A'-'a');
		i++;
	}
	printf("\n");
}

int main(void) {
	char str[] = "string";

	low_up(str);

	return 0;
}
