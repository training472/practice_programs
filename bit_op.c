#include <stdio.h>

void bit_set(int a, int key) {
	int res = a | 1 << key;

	printf("Bit Set: %d \n", res);
}

void bit_clear(int a, int key) {
	int res = a & ~(1 << key);

	printf("Bit Clear: %d \n", res);
}

void bit_toggle(int a, int key) {
	int res = a ^ 1 << key;

	printf("Bit Toggle: %d \n", res);
}

int main(void) {
	int a = 10;
	int key = 3;
	
	bit_set(a, key);
	bit_clear(a, key);
	bit_toggle(a, key);	

	return 0;
}
