/*#include <stdio.h>

#define SIZE 1
#define NUMLEN 1

int main(void)
{
	int buff[3] = {100, 101, 102};

	FILE *fp = fopen("output.txt", "wb");
	
	(&buff, 4, NUMLEN, fp);
	
	//printf("%d \n", buff);

	fclose(fp);

	return 0;
}

*/

// C program for writing
// struct to file
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// a struct to read and write
struct person
{
	int id;
	char fname[20];
	char lname[20];
};

int main ()
{
	FILE *outfile;

	// open file for writing
	outfile = fopen ("output.txt", "w");
	if (outfile == NULL)
	{
		fprintf(stderr, "\nError opened file\n");
		exit (1);
	}

	struct person input1 = {1, "rohan", "sharma"};
	struct person input2 = {2, "mahendra", "dhoni"};

	// write struct to file
	fwrite (&input1, sizeof(struct person), 1, outfile);
	fwrite (&input2, sizeof(struct person), 1, outfile);

	if(fwrite != 0)
		printf("contents to file written successfully !\n");
	else
		printf("error writing file !\n");

	// close file
	fclose (outfile);

	return 0;
}

