#include <stdio.h>

#define MAXSIZE 32

int main(void)
{
	char buf[MAXSIZE];

	FILE *fp = fopen("input.txt", "r");

	fread(buf, sizeof(buf), 1, fp);

	printf("%s \n", buf);

	fclose(fp);

	return 0;
}
