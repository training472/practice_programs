#include <stdio.h>
#include <stdarg.h>

int min(int count, ...)
{
	int min, a;
	
	va_list ap;

	va_start(ap, count);

	for(int i = 2; i <= count; i++) {
		if((a = va_arg(ap, int)) < min)
			min = a;
	}

	va_end(ap);

	return min;
}

int main(void)
{
	int count = 6;

	printf("%d \n", min(count, 10, 3, 4, 5, 1, 30));

	return 0;
}
