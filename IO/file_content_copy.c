#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *fp1, *fp2;

	char filename[20], c;

	fprintf(stdout, "Enter the file name: ");
	scanf("%s", filename);
	
	if ((fp1 = fopen(filename, "r")) == NULL) {
		fprintf(stderr, "Error has occured in file 1!!!!!!!!!!!!!!");
		exit(1);
	}
	

	fprintf(stdout, "Enter the file name: ");
	scanf("%s", filename);

	if ((fp2 = fopen(filename, "a")) == NULL) {
		fprintf(stderr, "Error has occured in file 2!!!!!!!!!!!!");
		exit(1);
	}

	while((c = getc(fp1)) != EOF)
		putc(c, fp2);

	fclose(fp2);
	fclose(fp1);

	return 0;
}
