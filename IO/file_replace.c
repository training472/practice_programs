#include <stdio.h>

int main(void)
{
	FILE *fp;

	char c;

	if ((fp = fopen("output.txt", "r+")) != NULL) {
		while((c = getc(fp)) != EOF) {
			if(c == 'i') {
				fseek(fp, -1, SEEK_CUR);
				putc('0', fp);
			}
		}
	}

	fclose(fp);

	return 0;
}
