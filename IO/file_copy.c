#include <stdio.h>

int main(void)
{
	char c;	

	FILE *fp1 = fopen("input.txt", "r");
	FILE *fp2 = fopen("output.txt", "a");

	while ((c = fgetc(fp1)) != EOF) {
		fputc(c, fp2);
	}
	
	fclose(fp2);
	fclose(fp1);

	return 0;
}
