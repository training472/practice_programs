#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	FILE *fp = fopen("input.txt", "r");
	FILE *fptr = fopen("output.txt", "w");

	char *line = "Hello World. \nTest file\n";
	char str[100];


	fgets(str, 100, fp);
	fputs(str, fptr);

	if(ferror(stdout)) {
		fprintf(stderr, "Error writing file...\n");
		exit(1);
	}
	
	system("ls");

	fclose(fptr);
	fclose(fp);

	return 0;
}
