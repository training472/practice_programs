#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char c, rep = 'A';
	char *filename = "input.txt";

	FILE *fp = fopen(filename, "w+");
	
	while ((c = fgetc(fp)) != EOF) {
		if (c == 'i') {
			fseek(fp, -1, SEEK_CUR);
			fputc(rep, fp);
		}
	}

	fclose(fp);

	return 0;
}
