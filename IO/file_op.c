#include <stdio.h>

int main(void)
{
	FILE *fp = fopen("input.txt", "r");
	FILE *fptr = fopen("output.txt", "w");
	
	char c;

	while ((c = getc(fp)) != EOF)
		putc(c, fptr);

	fclose(fptr);
	fclose(fp);
	
	return 0;
}
