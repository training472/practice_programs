#include <stdio.h>
#include <string.h>

char *foo(char *);

int main(int argc, char **argv)
{
	foo(argv[1]);

	return 0;
}

char *foo(char *bar)
{
	char c[12];
	
	strcpy(c, bar);
	
	return c;
}
