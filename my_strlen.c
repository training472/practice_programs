#include <stdio.h>

int my_strlen(char *str) {
	int len = 0;
	
	while(*str != '\0') {
		str++;
		len++;
	}
	
	return len;
}

int main(void) {
	char *str = "Hello World";

	printf("%d \n", my_strlen(str));
	
	return 0;
}
