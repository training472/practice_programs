#include <stdio.h>

int main(void)
{
	int *(*ptr)[20];
	int x[20];
	for (int i = 0; i < 20; i++)
		x[i] = i;
	
	ptr = x;

	for (int i = 0; i < 20; i++)
		printf("%d ", *(ptr + i));

	printf("\n");
	return 0;
}
