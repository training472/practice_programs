#include <stdio.h>

int main(void)
{
	int a = 5;
	const int *p = &a;

	int b = 10;

	p = &b;
	*p = 20;

	return 0;
}
