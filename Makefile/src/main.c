#include <stdio.h>
#include <stdlib.h>
#include "header.h"

int main(void)
{
	int a = 10, b = 20, ch;

	while (1) {
		printf("Enter your choice: \n");
		printf("1. Add\n");
		printf("2. Sub\n");
		printf("3. Mul\n");
		printf("4. Div\n");
		printf("5. Exit: ");

		scanf("%d",&ch);
		switch (ch) {
			case 1: printf("Sum: %d \n\n", add(a, b));
					break;
			case 2: printf("Diff: %d \n\n", sub(a, b));
					break;
			case 3: printf("Prod: %d \n\n", mul(a, b));
					break;
			case 4: if(a != 0)
						printf("Div: %d \n\n", divide(a, b));
					break;
			case 5: exit(0);
			default: printf("Invalid input!!!");
		}
	}
}
