#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
#include <poll.h>

#define PORT 8080
#define SIZE 1024
#define max_clients 10

int create_socket()
{
	int server_socket = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	addr.sin_addr.s_addr = inet_addr("192.168.1.202");

	int bindResult = bind(server_socket, (struct sockaddr *)&addr, sizeof(addr));

	if (bindResult == -1)
		perror("Bind Result");

	int listenResult = listen(server_socket, 5);

	if (listenResult == -1)
		perror("listenResult");

	printf("Server Start\n");

	return server_socket;
}

int wait_client(int server_socket)
{
	struct pollfd pollfds[max_clients+1];

	pollfds[0].fd = server_socket;
	pollfds[0].events = POLLIN | POLLPRI;

	int useClient = 0;
	int client_socket;

	while (1) {
		int pollResult = poll(pollfds, useClient+1, 5000);

		if (pollResult > 0) {
			if (pollfds[0].revents & POLLIN) {
				struct sockaddr_in cli_addr;

				int addrlen = sizeof(cli_addr);
				client_socket = accept(server_socket, (struct sockaddr *)&cli_addr, &addrlen);
				//printf("accept succuss: %s \n", inet_addr(cli_addr.sin_addr));

				for (int i = 1; i <= max_clients; i++) {
					if (pollfds[i].fd == 0) {
						pollfds[i].fd = client_socket;
						pollfds[i].events = POLLIN | POLLPRI;
						useClient++;
						break;
					}
				}
			}

			for (int i = 1; i <= max_clients; i++) {
				if (pollfds[i].fd > 0 && pollfds[i].revents & POLLIN) {
					char buf[SIZE];
					int bufsize = read(pollfds[i].fd, buf, SIZE - 1);
					if (bufsize == -1) {
						pollfds[i].fd = 0;
						pollfds[i].events = 0;
						pollfds[i].revents = 0;
						useClient--;
					}
					else if (bufsize == 0) {
						pollfds[i].fd = 0;
                                                pollfds[i].events = 0;
                                                pollfds[i].revents = 0;
                                                useClient--;
					} else {
						buf[bufsize] = '\0';

						FILE *p_fd;
						char cmd_buf[2048], cmd_out[2048] = {'\0'};

						if ((p_fd = popen(buf, "r")) == NULL) {
                					printf("Failed to run command!\n");
                					return -1;
        					} else {
                					while (fgets(cmd_buf, sizeof(cmd_buf), p_fd) != NULL) {
                        					strcat(cmd_out, cmd_buf);
                					}

                					send(pollfds[i].fd, cmd_out, strlen(cmd_out), 0);
                					printf("Command output sent...\n\n");
        					}

						//printf("From client: %s\n", buf);
					}
				}
			}
		}
	}
}

int main(void)
{
	int server_socket = create_socket();

	int client_socket = wait_client(server_socket);

	printf("server end\n");

	close(client_socket);
	close(server_socket);

	return 0;
}
