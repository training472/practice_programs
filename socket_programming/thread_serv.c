#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT 8080

int main(void)
{
	int sockfd, ret;
	struct sockaddr_in serverAddr;
	int clientSocket;
	struct sockaddr_in clientAddr;

	socklen_t addr_size;
	pid_t pid;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error in connection!!!\n");
		exit(1);
	}

	printf("Server Socket created...\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("192.168.1.202");

	if ((ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr))) < 0) {
		printf("Error in binding!!! \n");
		exit(1);
	}

	if (listen(sockfd, 10) == 0) {
		printf("Listening...\n");
	}

	int cnt = 0;
	while (1) {
		if((clientSocket = accept(sockfd, (struct sockaddr*)&clientAddr, &addr_size)) < 0)
			exit(1);

		printf("Connection accepted from %s: %d \n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));

		printf("Clients Connected: %d\n", ++cnt);

		if ((pid = fork()) == 0) {
			close(sockfd);

			send(clientSocket, "Hi client", strlen("Hi client"), 0);
		}

	}

	close(clientSocket);

	return 0;
}
