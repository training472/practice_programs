#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

#define PORT 8080

int main(void)
{
	int client_fd, valread, sock = 0;
	struct sockaddr_in serv_addr;
	char cmd[2048];
	char buffer[2048] = {'\0'};
	
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("\nSocket Creation error\n");
		exit(EXIT_FAILURE);
	}

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// converting ipv4 and ipv6 addr from text to binary
	if (inet_pton(AF_INET, "192.168.1.202", &serv_addr.sin_addr) <= 1) {
		printf("Invalid address Address not supported \n");
		return -1;
	}

	if ((client_fd = connect(sock, (struct sockaddr*)&serv_addr, sizeof(serv_addr))) < 0) {
		printf("Connection failed!\n");
		return -1;
	}

	printf("Enter the command: ");
	fgets(cmd, sizeof(cmd), stdin);

	send(sock, cmd, strlen(cmd), 0);
	printf("\nCommand output: \n");
	printf("----------------\n");
	valread = read(sock, buffer, 2048);
	printf("%s\n", buffer);

	close(client_fd);
	
	return 0;
}
