#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT 8080

int main(void)
{
	int clientSocket, ret;
	struct sockaddr_in serverAddr;
	char buffer[2048];

	if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Error in connection!!!\n");
		exit(1);
	}

	printf("Client socket is created...\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	memset(buffer, '\0', sizeof(buffer));

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("192.168.1.202");

	if ((ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr))) < 0) {
		printf("Error in connection!!!\n");\
			exit(1);
	}

	printf("Connected to server...\n");

	while (1) {
		if (recv(clientSocket, buffer, 2048, 0) < 0) {
			printf("Error while receiving data!!!\n");
		} else {
			printf("Server: %s \n", buffer);
			bzero(buffer, sizeof(buffer));
		}
	}

	return 0;
}
