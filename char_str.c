#include <stdio.h>

int main(void) {
//	char c = "x";	//Gives error due to string in place of char
	char c = 'x';

	printf("%c \n", c);	

	return 0;
}
