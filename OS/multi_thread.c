#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *fun(void *p)
{
	sleep(5);
	printf("child: %d\n", *(int *)p);
}

int main(void)
{
	pthread_t tid[3];

	clock_t t = clock();

	for (int i = 0; i < 3; i++) {
		printf("Thread %d :", i+1);
		pthread_create(&tid[i], NULL, fun, (void *)&tid[i]);
		pthread_join(tid[i], NULL);
	}

	//printf("Time: %f\n", ((double)(clock() - t))/CLOCKS_PER_SEC);
	pthread_exit(NULL);

	return 0;
}
