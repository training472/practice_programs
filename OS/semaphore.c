#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>

sem_t sem;

void *fun(void *p)
{
	sem_wait(&sem);
	printf("Entered...\n");

	sleep(4);

	printf("Finished...\n");
	sem_post(&sem);
}

int main(void)
{
	pthread_t t1, t2, t3;

	sem_init(&sem, 0, 2);
	
	pthread_create(&t1, NULL, fun, NULL);
	sleep(2);
	pthread_create(&t2, NULL, fun, NULL);
	sleep(2);
        pthread_create(&t3, NULL, fun, NULL);

	pthread_exit(0);

	sem_destroy(&sem);

	return 0;
}
