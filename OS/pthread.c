#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *fun(void *p)
{	
	//sleep(5);
	printf("Child thread: %ld \n", pthread_self());

	pthread_exit(0);
}

int main(void)
{
	pthread_t tid;

	pthread_create(&tid, NULL, fun, NULL);

	printf("Main thread: %ld \n", tid);
	
	//sleep(5);	
	pthread_join(tid, NULL);
	
	return 0;
}
