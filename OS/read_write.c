#include <stdio.h>
//#include <sys/types.h>
//#include <unistd.h>
#include <fcntl.h>

int main(void)
{
	int fd = creat("text.txt", O_RDWR);
	int x, len;

	char *c = (char *) calloc(100, sizeof(char));
	open(fd, O_RDWR);

	printf("File descriptor: %d \n", fd);
	if (fd == -1)
		printf("Error!!!\n");
	else {
		x = lseek(fd, 0, SEEK_SET);
	
		len = read(fd, c, 3);

		c[len] = '\0';

		printf("lseek: %s \n", c);
	}

	close(fd);

	return 0;
}
