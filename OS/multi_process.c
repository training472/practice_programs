#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
	pid_t pid[2], cid;
	int stat;

	for (int i = 0; i < 2; i++) {
		pid[i] = fork();
	}
	
	for (int i = 0; i < 2; i++) {
		if (!pid[i]) {
			printf("Child PID[%d]: %d\n", i+1, getpid());
		}
		else if (pid[i])
			cid = wait(NULL);
			printf("Parent PID[%d]: %d, Child terminated: %d\n\n\n", i+1, getpid(), cid);
	}

	return 0;
}
