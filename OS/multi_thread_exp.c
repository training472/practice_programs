#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void *fun(void *p)
{
	int i = 0;

	while (i++ < 10)
		printf("Thread: %d \t i: %d\n", *(int *)p, i);

	//printf("\n");
}

int main(void)
{
	pthread_t tid[3];

	for(int i = 0; i < 3; i++)
		pthread_create(&tid[i], NULL, fun, (void *)&tid[i]);

	pthread_exit(NULL);
	return 0;
}
