#include <stdio.h>
#include <pthread.h>
#include <unistd.h>


pthread_mutex_t lock;

void *fun(void *p)
{
	//pthread_mutex_lock(&lock);

	for (int i = 0; i < 10; i++) {
		printf("child: %d \t i: %d\n", *(int *)p, i);
		sleep(2);
	}

	printf("\n");
	//pthread_mutex_unlock(&lock);
}

int main(void)
{
	pthread_t tid[3];

	//pthread_mutex_init(&lock, NULL);

	for (int i = 0; i < 3; i++) { 
		pthread_create(&tid[2], NULL, fun, (void *)&tid[i]); 
	}

	pthread_exit(NULL);
	//pthread_mutex_destroy(&lock);

	return 0;
}
