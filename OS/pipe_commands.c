#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	char *cmd = "ls -lrt";
	char *awk = "awk '{if (NR != 1) printf $0 \"\\n\"}'";

	char command[50];
	
	sprintf(command, "%s | %s", cmd, awk);

	system(command);

	return 0;
}
