#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
	char *ls[] = {"/bin/ls","-lrt", NULL};

	pid_t pid = fork();

	if (pid == 0) {
		system("clear");
		execv(ls[0], ls);
	}
	else if (pid > 1) {
		wait(NULL);
		printf("Finished\n");
	}

	return 0;
}
