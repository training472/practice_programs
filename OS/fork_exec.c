#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
	pid_t pid;
	
	pid = fork();
	
	if (pid < 0) {
		printf("Fork failed!!!\n");
	}
	else if (pid == 0) {
		printf("Child process: %d \n", getpid());
		sleep(10);
		execlp("./test", "-1", NULL);
	}
	else {
		wait(NULL);
		printf("Parent process: %d \n", getpid());
	}

	return 0;
}
