#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(void)
{
	int fd1[2];
	int fd2[2];
	
	pid_t p;

	char fixed_str[100] = "ndra";
	char inp_str[100];

	if (pipe(fd1) == -1) {
		printf("Pipe failed.. \n");
		return 1;
	}

	if (pipe(fd2) == -1) {
		printf("Pipe failed... \n");
		return 1;
	}

	printf("Input: ");
	scanf("%s", inp_str);

	p = fork();

	if (p < 0) {
		printf("fork failed... \n");
		return 1;
	}
	// Parent
	else if (p > 0) {
		char con_cat[200];

		close(fd1[0]);

		write(fd1[1], inp_str, strlen(inp_str)+1);
		close(fd1[1]);

		wait(NULL);

		close(fd2[1]);
		
		read(fd2[0], con_cat, 200);
		printf("Concatenated: %s\n", con_cat);
		close(fd2[0]);
	}
	//Child
	else {
		char con_cat[200];
		close(fd1[1]);

		read(fd1[0], con_cat, 200);

		int k = strlen(con_cat);

		for (int i = 0; fixed_str[i] != '\0'; i++)
			con_cat[k++] = fixed_str[i];
	
		con_cat[k] = '\0';

		close(fd1[0]);
		close(fd2[0]);

		write(fd2[1], con_cat, strlen(con_cat)+1);
		close(fd2[1]);

		//exit(0);
	}
	
	return 0;
}
