#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
	pid_t pid;

	pid = vfork();

	if (pid == 0) {
		printf("child process: %d \n", getpid());
	}
	else if (pid > 0) {
		printf("parent process: %d \n", getpid());
	}
	else if (pid < 0)
		printf("Fork failed!!! \n");

	return 0;
}
