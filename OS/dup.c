#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

int main(void)
{
	int f = open("text.txt", O_RDONLY);

	int c = dup(f);

	printf("%d \t %d ", f, c);

	return 0;
}
