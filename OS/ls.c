#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

void _ls(char *dir, int op_a, int op_l)
{
	struct dirent *d;
	DIR *dh = opendir(dir);

	char cmd[500];
	char *command = "stat --format='%A %h %U %5s %.16z %n'";

	if (!dh) {
		if (errno == ENOENT)
			printf("Directory doen't exit");
		else
			printf("Unable to read directory");
		exit(EXIT_FAILURE);
	}

	while ((d = readdir(dh)) != NULL) {
		if (!op_a && d->d_name[0] == '.')
			continue;
		//printf("%s\t", d->d_name);

		if (op_l) {
			sprintf(cmd, "%s %s/%s", command, dir, d->d_name);
			system(cmd);
		}

		if (!op_l)
			printf("%s \n", d->d_name);
	}
	
	closedir(dh);
}

int main(int argc, char **argv)
{
	if (argc == 1)
		_ls(".", 0, 0);
	else if (argc == 2){
		if (argv[1][0] == '-') {
			int op_a = 0, op_l = 0;

			char *p = (char *)(argv[1] + 1);

			while (*p) {
				if (*p == 'a')
					op_a = 1;
				else if (*p == 'l')
					op_l = 1;
				else {
					printf("Option not available");
					exit(EXIT_FAILURE);
				}
				p++;
			}

			_ls(".", op_a, op_l);
		} else {
			_ls(argv[1], 0, 0);
		}
	}
	else if (argc == 3) {
		int op_a = 0, op_l = 0;

                        char *p = (char *)(argv[1] + 1);

                        while (*p) {
                                if (*p == 'a')
                                        op_a = 1;
                                else if (*p == 'l')
                                        op_l = 1;
                                else {
					printf("Came\n");
                                        printf("Option not available");
                                        exit(EXIT_FAILURE);
                                }
                                p++;
                        }

		_ls(argv[2], op_a, op_l);
	}

	return 0;
}
