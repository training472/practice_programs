#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
	pid_t pid_1, cid, pid_2;
	int status;

	pid_1 = fork();
	pid_2 = fork();

	if (!pid_1)
		printf("Child PID: %d\n", getpid());
	else if (pid_1) {
		printf("Status: %d \n", status);
		wait(&status);
		printf("Parent PID: %d, status: %d \n", getpid(), status);
	}
	else 
		printf("fork failed \n");

	return 0;
}
