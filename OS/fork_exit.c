#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(void)
{
	pid_t pid[5];
	int status;
	
	for (int i = 0; i < 2; i++) {
		pid[i] = fork();

		//sleep(3);

		//printf("Child: %d \n", getpid());
		exit(100 + i);
	}

	pid_t t = pid[0];

	for (int i = 0; i < 2; i++) {
		if (pid[i] == 0) {}
			//printf("Child: %d \n", getpid());
		else if (pid[i] < 0)
			printf("Fork failed!!!\n");
		else if (pid[i] > 0) {
			//waitpid(t, &status, WUNTRACED | WCONTINUED);
			if (WIFEXITED(status)) {
				printf("Exit status: %d \n", WEXITSTATUS(status));
				printf("Parent: %d\n", getpid());
			}
		}
	}

	return 0;
}
