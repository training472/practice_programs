#include <stdio.h>

#define DEF 40

#if a == 1
	#define VAL 10
#elif a == 2
	#define VAL 20
#endif

int main(void) {
	
	printf("%d \n", VAL);	

	printf("%d \n", DEF);
	
	#undef DEF

	#define DEF 30
	
	printf("%d \n", DEF);

	#ifdef DEF
		#define DEF 60
	#endif

	printf("%d \n", DEF);
	return 0;
}
