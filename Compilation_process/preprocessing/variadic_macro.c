#include <stdio.h>

#define eprintf(...) fprintf(stdout, __VA_ARGS__)

int main (void) {
	eprintf("%d %s \n", 10, "something");
	
	return 0;
}
