#include <stdio.h>

//auto int x = 10;

int add(int a, int b)
{
	return a + b;
}

int main(void)
{
	auto int x = 20;
	printf("%d \n", add(x, 30));

	return 0;
}
