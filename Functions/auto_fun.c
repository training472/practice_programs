#include <stdio.h>

auto void fun();

auto void fun()
{
	printf("fun \n");
}

int main(void)
{
	fun();

	return 0;
}
