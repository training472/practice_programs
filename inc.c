#include <stdio.h>

int main(void) {
	int n = 20;
	int a = ++n;
	int b = n++;

	printf("%d %d \n", a, b);

	return 0;
}
