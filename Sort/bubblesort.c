#include <stdio.h>
#include <stdlib.h>

void bubblesort(int arr[], int n)
{
	for (int i = 0; i < n - 1; i++) {
		for (int j = 0; j < n - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

void display(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", arr[i]);

	printf("\n");
}

int main(void)
{
	int n = 10;
	int arr[n];

	for (int i = 0 ; i < n; i++)
		arr[i] = rand() % 100;

	printf("Before sorting: ");
	display(arr, n);

	printf("After sorting: ");
	bubblesort(arr, n);

	display(arr, n);

	return 0;
}
