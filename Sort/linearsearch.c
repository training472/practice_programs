#include <stdio.h>

void linear_search(int a[], int n, int k)
{
	int flag = 0, pos;

	for (int i = 0; i < n; i++) {
		if (a[i] == k) {
			pos = i;
			flag = 1;
		}
	}

	if (flag)
		printf("%d is at the position: %d\n", k, pos);
	else 
		printf("%d not found...", k);
}

int main(void)
{
	int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int n = sizeof(a)/sizeof(a[0]);

	int k = 7;

	printf("Array: ");
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);
	printf("\n");

	linear_search(a, n, k);	

	return 0;
}
