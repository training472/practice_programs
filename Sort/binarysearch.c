#include <stdio.h>

int binsearch(int a[], int s, int e, int k)
{
	if (e >= 1) {
		int mid = (e + s) / 2;

		if (k == a[mid])
			return mid;
	
		else if (k < a[mid])
			return binsearch(a, s, mid-1, k);
		else
			return binsearch(a, mid+1, e, k);
	}
	return -1;
}

int main(void)
{
	int a[] = {1, 2, 3, 4, 5, 6, 7};
	int n = sizeof(a)/sizeof(a[0]);

	int k = 5;

	printf("Array: ");
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);
	printf("\n");

	int pos = binsearch(a, 0, n, k);

	if (pos != -1)
		printf("%d is at position: %d\n", k, pos);
	else
		printf("%d not found!!!\n", k);

	return 0;
}
