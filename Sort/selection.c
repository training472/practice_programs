#include <stdio.h>
#include <stdlib.h>

void selection(int a[], int n)
{
	int min, j;

	for (int i = 0; i < n - 1; i++) {
		min = i;

		for (j = i + 1; j < n; j++)
			if (a[j] < a[min])
				min = j;
	
		int temp = a[min];
		a[min] = a[i];
		a[i] = temp;
	}
}

void display(int a[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);

	printf("\n");
}

int main(void)
{
	int n = 10;
	int a[n];

	for (int i = 0; i < n; i++)
		a[i] = rand() % 100;

	printf("Before sort: ");
	display(a, n);

	selection(a, n);

	printf("After sort: ");
	display(a, n);

	return 0;
}
