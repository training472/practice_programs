#include <stdio.h>
#include <stdlib.h>

void insertion(int a[], int n)
{
	int key, j;
	for (int i = 1; i < n; i++) {
		key = a[i];
		j = i - 1;

		while (a[j] > key && j >= 0) {
			a[j+1] = a[j];
			--j;
		}

		a[j+1] = key;
	}
}

void display(int a[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);
	printf("\n");
}

int main(void)
{
	int n = 10;
	int a[n];

	for (int i = 0; i < n; i++)
		a[i] = rand() % 100;

	printf("Before sort: ");
	display(a, n);

	insertion(a, n);

	printf("After sort: ");
	display(a, n);

	return 0;
}
